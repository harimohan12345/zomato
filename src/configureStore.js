import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers/index';
import thunkMiddleware from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';


const configureStore = () => {
return createStore(rootReducer);
}

const makeStore = () => {
    return createStore(
	reducers,
	applyMiddleware(thunkMiddleware, promiseMiddleware),
)};

export default makeStore;