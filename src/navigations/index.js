import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../containers/HomeContainer';
import GraphsScreen from '../containers/Graphs';


const Stack = createStackNavigator();

function Navigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
     
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Graphs" component={GraphsScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Navigation;