import React, {useState, useEffect} from 'react';
import {View, Text, Image, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart
} from "react-native-chart-kit";

const { width, height } = Dimensions.get('window');

const chartConfig = {
  backgroundColor: "#e26a00",
  backgroundGradientFrom: "#fb8c00",
  backgroundGradientTo: "#ffa726",
  decimalPlaces: 2, 
  color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
  labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
  style: {
    borderRadius: 16
  },
  propsForDots: {
    r: "6",
    strokeWidth: "2",
    stroke: "#ffa726"
  }
}

let Graphs = () => {

  const data = {
    labels: ["January", "February", "March", "April", "May", "June"],
    datasets: [
      {
        data: [
          Math.random() * 100,
          Math.random() * 100,
          Math.random() * 100,
          Math.random() * 100,
          Math.random() * 100,
          Math.random() * 100
        ]
      }
    ]
  };
  return (
    <View style={styles.container}>
     <Text style={{fontSize: 20, fontWeight:'600'}} >Restaurant Sales Chart</Text>
  <LineChart
    data={data}
    width={width-25} 
    height={220}
    yAxisLabel="$"
    yAxisSuffix="k"
    yAxisInterval={1} 
    chartConfig={chartConfig}
    bezier
    style={{
      marginVertical: 8,
      borderRadius: 16
    }}
  />
  <BarChart
  //style={graphStyle}
  data={data}
  width={width-25}
  height={220}
  yAxisLabel="$"
  chartConfig={chartConfig}
  verticalLabelRotation={30}
/>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width:width,
    padding: 10
   
  }
});

export default Graphs;
