import React, { useState, useEffect } from 'react';
import { View, Text, Button, FlatList, Modal, StyleSheet, TextInput, TouchableHighlight, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeCount } from '../actions/index';
import { getRestaurantData, deleteRestaurant, editNameRestaurant } from '../actions/restaurant';
import EmptyState from '../compnonents/EmptyState';
import ListItem from '../compnonents/ListItemRestraunt';

const COUNT = 20;
const { width, height } = Dimensions.get('screen');

function HomeScreen({data, getRestaurantData, navigation, deleteRestaurant, editNameRestaurant}) {
  const [start, setStart] = useState(0);
  const [modalVisible, setModalVisible] = useState(false);
  const [name, setName] = useState("");
  const [id, setId] = useState("");
  useEffect(() => {
    // Update the document title using the browser API
    getRestaurantData({start, count: COUNT});
    setStart(20);
  }, [0]);
  handleLoadMore = () =>{
    getRestaurantData({start, count: COUNT});
    setStart(start+20);

  }
editName = (id, name) =>{
    setName(name);
    setModalVisible(!modalVisible);
    setId(id);

  }
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
       
       <FlatList
        data={data}
       
        renderItem={({ item, id }) => <ListItem navigation={navigation} editName={editName} deleteItem={(id=>deleteRestaurant(id))} {...item.restaurant} />}
        
        keyExtractor={item =>item.id}
        onEndReached={handleLoadMore}
        ListFooterComponent={() => <View style={{height: 100}} />}
        ListEmptyComponent={()=><EmptyState
          
          title={"Emtry Item"}
          subTitle={"No restaurant found"}
        />}
      />
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
       
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Edit Name</Text>
            <TextInput
      style={{ height: 40, width:width-100, borderColor: 'gray', borderWidth: 1 }}
      onChangeText={text => setName(text)}
      value={name}
    />
            <TouchableHighlight
              style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
              onPress={() => {
                editNameRestaurant({id:id, name: name})
                setModalVisible(!modalVisible);
              }}
            >
              <Text style={styles.textStyle}>Update Name</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
      </View>
    );
  }

  const mapStateToProps = state => ({
    count: state.count.count,
    data: state.restaurantReducer.data
  });

  const styles = StyleSheet.create({
    centeredView: {
      flex: 1,
      width: "90%",
      justifyContent: "center",
      alignItems: "center",
      marginTop: 22
    },
    modalView: {
      margin: 10,
      backgroundColor: "white",
      borderRadius: 10,
      padding: 10,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5
    },
    openButton: {
      backgroundColor: "#F194FF",
      borderRadius: 20,
      padding: 10,
      elevation: 2
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center"
    }
  });

export default connect(mapStateToProps, {getRestaurantData, deleteRestaurant, editNameRestaurant})(HomeScreen)