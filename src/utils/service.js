import axios from 'axios';

import {store} from '../index';
import { showLoaderScreen } from '../actions/loader';
let baseURL = {
  GATEWAY_URL: 'https://developers.zomato.com'
}

const mishPayServiceInstance = axios.create({
  baseURL: baseURL.GATEWAY_URL,
  timeout: 30000,
});


const requestInterceptor = (config) => {
 let copyConfig = {};
  copyConfig.headers = {...config.headers, ...global.headerValues
  };
  copyConfig.baseURL = baseURL.GATEWAY_URL;
  // delete copyConfig.data;
  store.dispatch(showLoaderScreen(true));
  
  return {...config, ...copyConfig};
}

const requestInterceptorError = (error) => {
  return Promise.reject(error);
}

const responseInterceptor = (res) => {
 store.dispatch(showLoaderScreen(false))
 console.log("resresres", res);
 return res
};

const responseInterceptorError = (error) => {
  
  store.dispatch(showLoaderScreen(false))
 //we can write some bridge call for toast message;
 alert("Some thing went wrong")
  return Promise.reject(error);
}


mishPayServiceInstance.interceptors.request.use(requestInterceptor, requestInterceptorError);

mishPayServiceInstance.interceptors.response.use(responseInterceptor, responseInterceptorError);

export { mishPayServiceInstance }