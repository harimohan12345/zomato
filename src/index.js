import React from 'react';
import { View, StyleSheet } from 'react-native';
import Navigation from './navigations';
import {Provider} from 'react-redux';
import Loader from './compnonents/Loader';
import configureStore from './configureStore';

export const store = configureStore();



const AppView = () => {

  return (
    <View style={styles.container} >
      <Navigation screenProps={{}} />
      <Loader />
      {/* <View>
        Global error and success message view
      </View> */}
    </View>
  )
}

const AppContainer = () => {
  return (
    <Provider store={store}>
      <AppView />
    </Provider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
});


export default AppContainer;
