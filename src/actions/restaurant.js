import { GET_DATA_RESTAURANT, DELETE_DATA_RESTAURANT, EDIT_NAME_DATA_RESTAURANT } from '../constants/restaurant';
import {mishPayServiceInstance} from '../utils/service';

export const getRestaurantData = (val) => {
    return ({
        type: GET_DATA_RESTAURANT,
        payload: ()=>mishPayServiceInstance.get('/api/v2.1/search?entity_id=1&entity_type=city&start='+val.start+'&count='+val.count)
      });

}

export const deleteRestaurant = (val) => {
    return ({
        type: DELETE_DATA_RESTAURANT,
        payload: {id:val}
      });

}

export const editNameRestaurant = (val) => {
    return ({
        type: EDIT_NAME_DATA_RESTAURANT,
        payload: val
      });

}
  
  

  