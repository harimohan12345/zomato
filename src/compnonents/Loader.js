import React, { Component } from 'react';
import { View, Dimensions, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';

const { width, height } = Dimensions.get('window');

const Loader = ({ showLoader, forceLoader }) => {
 return (
  showLoader || forceLoader ?
   <View style={{
    width: width,
    height: height,
    backgroundColor: 'rgba(255,255,255,0.8)',
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 100,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
   }}>
    <ActivityIndicator size="large" color="#0000ff" />
   </View>
   : null
 )
}
const mapStateToProps = ({ loaderReducer }) => ({
 showLoader: loaderReducer.showLoader,
});

export default connect(mapStateToProps, null)(Loader);

