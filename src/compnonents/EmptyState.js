import React, {Component} from 'react';
import {View, Dimensions, Text} from 'react-native';
import PropTypes from 'prop-types';
const {width, height} = Dimensions.get('window');

const EmptyState = ({title, subTitle}) => {
  return (
    <View
      style={{
        width: width,
        height: height,
        backgroundColor: 'rgba(255,255,255,0.8)',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text style={{size: 18, color: 'black', fontWeight:'600'}}>{title}</Text>
      <Text style={{size: 18, color: 'gray', marginTop: 20}}>{subTitle}</Text>
    </View>
  );
};
EmptyState.propTypes = {
  title: PropTypes.string,
  subTitle: PropTypes.string,
};

EmptyState.defaultProps = {
  title: '',
  subTitle: '',
};

export default EmptyState;
