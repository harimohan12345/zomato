import React, {useState, useEffect} from 'react';
import {View, Text, Image, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';

const { width, height } = Dimensions.get('window');

let ListItemRestRaunt = ({name, location, navigation, id, thumb, editName, deleteItem}) => {
  return (
    <TouchableOpacity onPress={()=>navigation.push('Graphs')} ><View style={styles.container}>
      <View style={{width: 100}} >
        <Image
          style={styles.restLogo}
          source={{
            uri: thumb,
          }}
        />
      </View>
      <View style={{padding: 10, justifyContent: 'flex-start',
        alignSelf: 'flex-start', width: width-130}} >
        <Text style={styles.title} >{name}</Text>
        <Text style={styles.subTtile} >{location.address}</Text>
        <TouchableOpacity onPress={()=>deleteItem(id)} ><Text style={styles.cta} >Delete</Text></TouchableOpacity>
        <TouchableOpacity onPress={()=>editName(id, name)} ><Text style={styles.cta} >Edit Name</Text></TouchableOpacity>
      
        
      </View>
    </View></TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width:width-20,
    
    marginTop: 10,
    backgroundColor: '#F5FCFF',
    flexDirection: 'row',
  },
  title: {
    fontSize: 18,
    fontWeight: '600',
    color: 'black',
  },
  subTtile: {
    fontSize: 12,
    fontWeight: '400',
    color: 'gray',
    flex: 1, flexWrap: 'wrap',
    textAlign:'justify'
  },
  restLogo: {
    width: 100,
    height: 100,
    resizeMode: 'contain',
  },
  cta:{
      color:'black',
      fontSize: 18,
      fontWeight: '600'
  }
});

export default ListItemRestRaunt;
