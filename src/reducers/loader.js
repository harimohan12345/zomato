const loaderReducer = (state = {showLoader: false}, action) =>{
  switch(action.type){
    case "LOADER": return {...state, showLoader: action.showLoader || false}
    default: return {...state}
  }
}


export default loaderReducer;
