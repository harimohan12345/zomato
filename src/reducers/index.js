import { COUNTER_CHANGE } from '../constants';
import { combineReducers } from 'redux';
import loaderReducer from './loader';
import restaurantReducer from './restaurant';


const initialState = {
count: 0
};
const countReducer = (state = initialState, action) => {
switch(action.type) {
case COUNTER_CHANGE:
return {
...state,
count:action.payload
};
default:
return state;
}
}

const rootReducer = combineReducers(
    { count: countReducer, loaderReducer, restaurantReducer }
    );

    

export default rootReducer;