import typeToReducer from 'type-to-reducer';

import { GET_DATA_RESTAURANT, DELETE_DATA_RESTAURANT, EDIT_NAME_DATA_RESTAURANT } from '../constants/restaurant';

const initialState = {
  ui: {
    loading: false,
  },
  data: [],
  error: '',
};

const restaurantReducer = typeToReducer({
  [GET_DATA_RESTAURANT]: {
    PENDING: state => {
      return Object.assign({}, state, { ui: { loading: true } });
    },
    REJECTED: (state, action) => {
      return Object.assign({}, state, { error: action.payload.message, ui: { loading: false } })
    },
    FULFILLED: (state, action) => {
      return Object.assign({}, state, {
       data:[...state.data, ...action.payload.data.restaurants],
        ui: { loading: false } });
    }},
    [DELETE_DATA_RESTAURANT]: (state, action) => {
      return Object.assign({}, state, {
        data:state.data.filter(item=>item.restaurant.id != action.payload.id),
         ui: { loading: false } });
     
  },
  [EDIT_NAME_DATA_RESTAURANT]: (state, action) => {
    return Object.assign({}, state, {
      data:state.data.map(item=>item.restaurant.id == action.payload.id ? {...item, restaurant: {...item.restaurant, name:action.payload.name}} : item),
       ui: { loading: false } });
   
}}, initialState);

  export default restaurantReducer;